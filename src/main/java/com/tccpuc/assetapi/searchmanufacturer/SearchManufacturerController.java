package com.tccpuc.assetapi.searchmanufacturer;

import com.tccpuc.assetapi.searchmanufacturer.mapper.ManufacturerMapper;
import com.tccpuc.assetapi.searchmanufacturer.dto.SearchManufacturerResponse;
import com.tccpuc.assetapi.searchmanufacturer.service.SearchManufacturerService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class SearchManufacturerController {
  private final SearchManufacturerService searchManufacturerService;
  private final ManufacturerMapper manufacturerMapper;

  @GetMapping("/manufacturers")
  public List<SearchManufacturerResponse> searchAllManufacturers() {
    return searchManufacturerService.findAllManufacturers()
        .stream()
        .map(manufacturerMapper::toSearchManufacturerResponse)
        .collect(Collectors.toList());
  }


}
