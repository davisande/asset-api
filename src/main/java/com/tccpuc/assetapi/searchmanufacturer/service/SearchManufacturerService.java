package com.tccpuc.assetapi.searchmanufacturer.service;

import com.tccpuc.assetapi.core.exception.NotFoundException;
import com.tccpuc.assetapi.core.repository.ManufacturerRepository;
import com.tccpuc.assetapi.searchmanufacturer.dto.Manufacturer;
import com.tccpuc.assetapi.searchmanufacturer.mapper.ManufacturerMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SearchManufacturerService {

  private final ManufacturerRepository manufacturerRepository;
  private final ManufacturerMapper manufacturerMapper;

  public List<Manufacturer> findAllManufacturers() {
    return Optional.of(performFindAllManufacturers())
        .filter(manufacturersList -> !manufacturersList.isEmpty())
        .orElseThrow(NotFoundException::new);
  }

  private List<Manufacturer> performFindAllManufacturers() {
    return manufacturerRepository.findAll(Sort.by("name"))
        .stream()
        .map(manufacturerMapper::fromManufacturerEntity)
        .collect(Collectors.toList());
  }

}
