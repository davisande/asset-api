package com.tccpuc.assetapi.searchmanufacturer.mapper;

import com.tccpuc.assetapi.core.entity.ManufacturerEntity;
import com.tccpuc.assetapi.searchmanufacturer.dto.Manufacturer;
import com.tccpuc.assetapi.searchmanufacturer.dto.SearchManufacturerResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ManufacturerMapper {

  Manufacturer fromManufacturerEntity(ManufacturerEntity manufacturerEntity);

  SearchManufacturerResponse toSearchManufacturerResponse(Manufacturer manufacturer);

}
