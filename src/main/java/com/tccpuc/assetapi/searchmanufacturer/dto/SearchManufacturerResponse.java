package com.tccpuc.assetapi.searchmanufacturer.dto;

import lombok.Data;

@Data
public class SearchManufacturerResponse {

  private Integer id;
  private String name;

}
