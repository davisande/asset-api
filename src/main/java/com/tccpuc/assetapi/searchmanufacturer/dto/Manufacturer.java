package com.tccpuc.assetapi.searchmanufacturer.dto;

import lombok.Data;

@Data
public class Manufacturer {

  private Integer id;
  private String name;

}
