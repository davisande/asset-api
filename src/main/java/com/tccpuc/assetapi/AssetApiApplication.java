package com.tccpuc.assetapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class AssetApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssetApiApplication.class, args);
	}

}
