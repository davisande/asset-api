package com.tccpuc.assetapi.asset.dto;

import com.tccpuc.assetapi.core.enums.ProductTypeEnum;
import lombok.Data;

@Data
public class ProductResponse {

  private Integer id;
  private String name;
  private ProductTypeEnum type;
  private ManufacturerResponse manufacturer;

}
