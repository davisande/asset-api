package com.tccpuc.assetapi.asset.dto;

import lombok.Data;

@Data
public class ManufacturerResponse {

  private Integer id;
  private String name;

}
