package com.tccpuc.assetapi.asset.searchasset.exception;

public class SearchAssetException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Error searching asset";

  public SearchAssetException() {
    super(DEFAULT_MESSAGE);
  }
}
