package com.tccpuc.assetapi.asset.searchasset;

import com.tccpuc.assetapi.asset.mapper.AssetMapper;
import com.tccpuc.assetapi.asset.searchasset.dto.SearchAssetResponse;
import com.tccpuc.assetapi.asset.searchasset.exception.SearchAssetException;
import com.tccpuc.assetapi.asset.searchasset.service.SearchAssetService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class SearchAssetController {

  private final SearchAssetService searchAssetService;
  private final AssetMapper assetMapper;

  @GetMapping("/assets")
  public List<SearchAssetResponse> searchAllAssets() {
    return Optional.of(searchAssetService.searchAllAssets())
        .map(assetMapper::toSearchAssetResponseList)
        .orElseThrow(SearchAssetException::new);
  }

}
