package com.tccpuc.assetapi.asset.searchasset.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.tccpuc.assetapi.asset.dto.ProductResponse;
import java.time.LocalDateTime;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class SearchAssetResponse {

  private String maintenancePeriodicity;
  private Integer amountMaintenancePeriod;
  private LocalDateTime purchaseDate;
  private LocalDateTime lastMaintenanceDate;
  private String note;
  private LocalDateTime creationDate;
  private LocalDateTime updateDate;
  private ProductResponse product;

}
