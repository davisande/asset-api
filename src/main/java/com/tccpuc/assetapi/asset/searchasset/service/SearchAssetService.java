package com.tccpuc.assetapi.asset.searchasset.service;

import com.tccpuc.assetapi.asset.domain.Asset;
import com.tccpuc.assetapi.asset.mapper.AssetMapper;
import com.tccpuc.assetapi.core.exception.NotFoundException;
import com.tccpuc.assetapi.core.repository.AssetRepository;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SearchAssetService {

  private final AssetRepository assetRepository;
  private final AssetMapper assetMapper;

  public List<Asset> searchAllAssets() {
    return Optional.ofNullable(assetRepository.findAll())
        .map(assetMapper::fromAssetEntity)
        .orElseThrow(NotFoundException::new);
  }

}
