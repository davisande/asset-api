package com.tccpuc.assetapi.asset.cretateasset.dto.request;

import com.tccpuc.assetapi.core.enums.MaintenancePeriodicityEnum;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class CreateAssetRequest {

  @NonNull
  private MaintenancePeriodicityEnum maintenancePeriodicity;

  @NonNull
  private Integer amountMaintenancePeriod;

  @NonNull
  private LocalDateTime purchaseDate;

  private LocalDateTime lastMaintenanceDate;

  private String note;

  @NonNull
  private ProductRequest product;

}
