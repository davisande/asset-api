package com.tccpuc.assetapi.asset.cretateasset.service;

import static java.util.Optional.ofNullable;

import com.tccpuc.assetapi.asset.cretateasset.exception.CreateAssetException;
import com.tccpuc.assetapi.core.entity.AssetEntity;
import com.tccpuc.assetapi.core.entity.ProductEntity;
import com.tccpuc.assetapi.core.repository.AssetRepository;
import com.tccpuc.assetapi.core.repository.ProductRepository;
import com.tccpuc.assetapi.asset.domain.Asset;
import com.tccpuc.assetapi.asset.mapper.AssetMapper;
import java.util.Optional;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CreateAssetService {

  private final AssetMapper assetMapper;
  private final ProductRepository productRepository;
  private final AssetRepository assetRepository;

  public Asset createAsset(@NonNull final Asset asset) {
    return ofNullable(asset.getProduct().getId())
        .map(productRepository::findById)
        .map(optionalProductEntity -> buildAssetEntity(asset, optionalProductEntity))
        .map(assetRepository::save)
        .map(assetMapper::fromAssetEntity)
        .orElseThrow(CreateAssetException::new);
  }

  private AssetEntity buildAssetEntity(final Asset asset,
      final Optional<ProductEntity> optionalProductEntity) {
    return optionalProductEntity
        .map(productEntity -> fillAssetEntity(asset, productEntity))
        .orElseThrow(CreateAssetException::new);
  }

  private AssetEntity fillAssetEntity(final Asset asset, final ProductEntity productEntity) {
    final AssetEntity assetEntity = assetMapper.toAssetEntity(asset);
    assetEntity.setProduct(productEntity);

    return assetEntity;
  }

}
