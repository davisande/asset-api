package com.tccpuc.assetapi.asset.cretateasset;

import static java.util.Optional.of;

import com.tccpuc.assetapi.asset.cretateasset.dto.request.CreateAssetRequest;
import com.tccpuc.assetapi.asset.cretateasset.exception.CreateAssetException;
import com.tccpuc.assetapi.asset.cretateasset.service.CreateAssetService;
import com.tccpuc.assetapi.asset.cretateasset.dto.response.CreateAssetResponse;
import com.tccpuc.assetapi.asset.mapper.AssetMapper;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class CreateAssetController {

  private final AssetMapper assetMapper;
  private final CreateAssetService createAssetService;

  @PostMapping("/assets")
  public ResponseEntity<CreateAssetResponse> createAsset(
      @RequestBody @Valid CreateAssetRequest createAssetRequest) {
    return of(createAssetRequest)
        .map(assetMapper::fromCreateAssetRequest)
        .map(createAssetService::createAsset)
        .map(assetMapper::toCreateAssetResponse)
        .map(this::buildResponse)
        .orElseThrow(CreateAssetException::new);
  }

  private ResponseEntity<CreateAssetResponse> buildResponse(
      CreateAssetResponse createAssetResponse) {
    return new ResponseEntity<>(createAssetResponse, HttpStatus.CREATED);
  }

}
