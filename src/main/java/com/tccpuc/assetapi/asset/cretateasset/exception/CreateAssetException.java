package com.tccpuc.assetapi.asset.cretateasset.exception;

public class CreateAssetException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Error creating asset";

  public CreateAssetException() {
    super(DEFAULT_MESSAGE);
  }
}
