package com.tccpuc.assetapi.asset.cretateasset.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class ProductRequest {

  @NonNull
  private Integer id;

}
