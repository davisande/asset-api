package com.tccpuc.assetapi.asset.mapper;

import static java.util.Optional.ofNullable;

import com.tccpuc.assetapi.asset.cretateasset.dto.request.CreateAssetRequest;
import com.tccpuc.assetapi.asset.cretateasset.dto.response.CreateAssetResponse;
import com.tccpuc.assetapi.asset.searchasset.dto.SearchAssetResponse;
import com.tccpuc.assetapi.core.entity.AssetEntity;
import com.tccpuc.assetapi.asset.domain.Asset;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class AssetMapper {

  public abstract AssetEntity toAssetEntity(Asset asset);

  public abstract Asset fromAssetEntity(AssetEntity assetEntity);

  public abstract List<Asset> fromAssetEntity(List<AssetEntity> assetsEntity);

  public abstract Asset fromCreateAssetRequest(CreateAssetRequest createAssetRequest);

  @Mapping(source = "maintenancePeriodicity", target = "maintenancePeriodicity",
      qualifiedByName = "getNameMaintenancePeriodicity")
  public abstract CreateAssetResponse toCreateAssetResponse(Asset asset);

  @Mapping(source = "maintenancePeriodicity", target = "maintenancePeriodicity",
      qualifiedByName = "getNameMaintenancePeriodicity")
  public abstract SearchAssetResponse toSearchAssetResponse(Asset asset);

  public List<SearchAssetResponse> toSearchAssetResponseList(@NonNull final List<Asset> assetList) {
    return assetList.stream()
        .map(this::toSearchAssetResponse)
        .collect(Collectors.toList());
  }

  private String getNameMaintenancePeriodicity(final Asset asset) {
    return ofNullable(asset)
        .map(a -> a.getMaintenancePeriodicity().getName())
        .orElse(null);
  }

}
