package com.tccpuc.assetapi.asset.domain;

import lombok.Data;

@Data
public class Manufacturer {

  private Integer id;
  private String name;

}
