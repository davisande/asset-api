package com.tccpuc.assetapi.asset.domain;

import com.tccpuc.assetapi.core.enums.MaintenancePeriodicityEnum;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class Asset {

  private Integer id;
  private MaintenancePeriodicityEnum maintenancePeriodicity;
  private Integer amountMaintenancePeriod;
  private LocalDateTime purchaseDate;
  private LocalDateTime lastMaintenanceDate;
  private String note;
  private LocalDateTime creationDate;
  private LocalDateTime updateDate;
  private Product product;

}
