package com.tccpuc.assetapi.searchproduct.dto;

import lombok.Data;

@Data
public class SearchProductResponse {

  private Integer id;
  private String name;
  private String type;

}
