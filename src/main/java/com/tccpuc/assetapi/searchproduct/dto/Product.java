package com.tccpuc.assetapi.searchproduct.dto;

import com.tccpuc.assetapi.core.enums.ProductTypeEnum;
import lombok.Data;

@Data
public class Product {

  private Integer id;
  private String name;
  private ProductTypeEnum type;

}
