package com.tccpuc.assetapi.searchproduct.mapper;

import com.tccpuc.assetapi.core.entity.ProductEntity;
import com.tccpuc.assetapi.searchproduct.dto.Product;
import com.tccpuc.assetapi.searchproduct.dto.SearchProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapper {

  Product fromProductEntity(ProductEntity productEntity);

  @Mapping(source = "type.description", target = "type")
  SearchProductResponse toSearchProductResponse(Product product);

}
