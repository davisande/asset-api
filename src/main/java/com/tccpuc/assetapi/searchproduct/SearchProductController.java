package com.tccpuc.assetapi.searchproduct;

import com.tccpuc.assetapi.searchproduct.mapper.ProductMapper;
import com.tccpuc.assetapi.searchproduct.dto.SearchProductResponse;
import com.tccpuc.assetapi.searchproduct.service.SearchProductService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class SearchProductController {

  private final SearchProductService searchProductService;
  private final ProductMapper productMapper;

  @GetMapping("/manufacturers/{manufacturerId}/products")
  public List<SearchProductResponse> searchProductsByManufacturerId(
      @PathVariable final Integer manufacturerId) {
    return searchProductService.findProductsByManufacturerId(manufacturerId)
        .stream()
        .map(productMapper::toSearchProductResponse)
        .collect(Collectors.toList());
  }
}
