package com.tccpuc.assetapi.searchproduct.service;

import com.tccpuc.assetapi.core.entity.ProductEntity;
import com.tccpuc.assetapi.core.exception.NotFoundException;
import com.tccpuc.assetapi.core.repository.ManufacturerRepository;
import com.tccpuc.assetapi.core.repository.ProductRepository;
import com.tccpuc.assetapi.searchproduct.dto.Product;
import com.tccpuc.assetapi.searchproduct.mapper.ProductMapper;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SearchProductService {

  private final ManufacturerRepository manufacturerRepository;
  private final ProductRepository productRepository;
  private final ProductMapper productMapper;

  public List<Product> findProductsByManufacturerId(@NonNull final Integer manufacturerId) {
    return manufacturerRepository.findById(manufacturerId)
        .map(productRepository::findByManufacturerOrderByNameAsc)
        .map(this::convertEntityInDomain)
        .filter(productList -> !productList.isEmpty())
        .orElseThrow(NotFoundException::new);
  }

  private List<Product> convertEntityInDomain(
      @NonNull final List<ProductEntity> productEntityList) {
    return productEntityList
        .stream()
        .map(productMapper::fromProductEntity)
        .collect(Collectors.toList());

  }


}
