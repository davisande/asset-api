package com.tccpuc.assetapi.core.enums;

import java.util.stream.Stream;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ProductTypeEnum {
  E("Equipamento"),
  P("Peça"),
  I("Insumo");

  private String description;
}

