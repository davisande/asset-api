package com.tccpuc.assetapi.core.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum MaintenancePeriodicityEnum {
  DA("Daily"),
  WE("Weekly"),
  YE("Yearly");

  private String name;

}
