package com.tccpuc.assetapi.core.exception;

public class NotFoundException extends RuntimeException {
  private static final String DEFAULT_MESSAGE = "Resource not found";

  public NotFoundException() {
    super(DEFAULT_MESSAGE);
  }
}
