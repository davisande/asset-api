package com.tccpuc.assetapi.core.advice;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Errors {

  private List<String> messages;

}
