package com.tccpuc.assetapi.core.entity;

import com.tccpuc.assetapi.core.enums.ProductTypeEnum;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "product")
@EntityListeners(AuditingEntityListener.class)
public class ProductEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @Column
  private String name;

  @Enumerated(EnumType.STRING)
  private ProductTypeEnum type;

  @ManyToOne
  private ManufacturerEntity manufacturer;

  @CreatedDate
  @Column(name = "creation_date")
  private LocalDateTime creationDate;

  @LastModifiedDate
  @Column(name = "update_date")
  private LocalDateTime updateDate;

}
