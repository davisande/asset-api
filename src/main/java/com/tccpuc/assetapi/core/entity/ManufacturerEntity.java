package com.tccpuc.assetapi.core.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "manufacturer")
@EntityListeners(AuditingEntityListener.class)
public class ManufacturerEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @Column
  private String name;

  @CreatedDate
  @Column(name = "creation_date")
  private LocalDateTime creationDate;

  @LastModifiedDate
  @Column(name = "update_date")
  private LocalDateTime updateDate;

}
