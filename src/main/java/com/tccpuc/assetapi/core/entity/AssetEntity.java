package com.tccpuc.assetapi.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import com.tccpuc.assetapi.core.enums.MaintenancePeriodicityEnum;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "asset")
@EntityListeners(AuditingEntityListener.class)
public class AssetEntity {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  private MaintenancePeriodicityEnum maintenancePeriodicity;

  @Column(name = "amount_maintenance_period")
  private Integer amountMaintenancePeriod;

  @Column(name = "purchase_date")
  private LocalDateTime purchaseDate;

  @Column(name = "last_maintenance_date")
  private LocalDateTime lastMaintenanceDate;

  @Column
  private String note;

  @CreatedDate
  @Column(name = "creation_date")
  private LocalDateTime creationDate;

  @LastModifiedDate
  @Column(name = "update_date")
  private LocalDateTime updateDate;

  @ManyToOne
  private ProductEntity product;

}
