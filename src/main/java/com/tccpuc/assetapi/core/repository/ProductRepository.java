package com.tccpuc.assetapi.core.repository;

import com.tccpuc.assetapi.core.entity.ManufacturerEntity;
import com.tccpuc.assetapi.core.entity.ProductEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {

  List<ProductEntity> findByManufacturerOrderByNameAsc(ManufacturerEntity manufacturerEntity);

}
