package com.tccpuc.assetapi.core.repository;

import com.tccpuc.assetapi.core.entity.AssetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssetRepository extends JpaRepository<AssetEntity, Integer> {

}
