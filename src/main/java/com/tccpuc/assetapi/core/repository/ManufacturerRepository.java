package com.tccpuc.assetapi.core.repository;

import com.tccpuc.assetapi.core.entity.ManufacturerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<ManufacturerEntity, Integer> {

}
