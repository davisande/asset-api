INSERT INTO product (manufacturer_id, name, type, creation_date) VALUES
((SELECT id FROM manufacturer where name = 'Furlan'), 'Alimentador vibratório', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Furlan'), 'Britador de impacto VSI', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Furlan'), 'Moinho de rolo', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Furlan'), 'Peneira e grelha vibratória', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Furlan'), 'Tranportador de correia', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Brastorno'), 'Classificador espiral', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Bercam'), 'Rebritador de mandíbula', 'E', NOW()),
((SELECT id FROM manufacturer where name = 'Odebraz'), 'Chute de descargue', 'P', NOW()),
((SELECT id FROM manufacturer where name = 'Odebraz'), 'Deck', 'P', NOW()),
((SELECT id FROM manufacturer where name = 'Odebraz'), 'Pinhão', 'P', NOW());