CREATE TABLE manufacturer (
	id INTEGER NOT NULL AUTO_INCREMENT,
	name VARCHAR(150) NULL,
	creation_date TIMESTAMP NOT NULL,
  update_date TIMESTAMP NULL,
  CONSTRAINT pk_manufacturer PRIMARY KEY (id)
);

CREATE TABLE product (
	id INTEGER NOT NULL AUTO_INCREMENT,
	manufacturer_id INTEGER NOT NULL,
	name VARCHAR(150) NULL,
	type CHAR(1) NOT NULL,
	creation_date TIMESTAMP NOT NULL,
  update_date TIMESTAMP NULL,
  CONSTRAINT pk_equipment PRIMARY KEY (id),
  CONSTRAINT fk_product_manufacturer FOREIGN KEY (manufacturer_id) REFERENCES manufacturer(id)
);

CREATE TABLE asset (
	id INTEGER NOT NULL AUTO_INCREMENT,
  product_id INTEGER NOT NULL,
	maintenance_periodicity CHAR(2) NULL,
	amount_maintenance_period INTEGER NULL,
	purchase_date TIMESTAMP NOT NULL,
	last_maintenance_date TIMESTAMP NULL,
	note VARCHAR(500) NULL,
	creation_date TIMESTAMP NULL,
	update_date TIMESTAMP NULL,
	CONSTRAINT pk_asset PRIMARY KEY (id),
	CONSTRAINT fk_asset_product FOREIGN KEY (product_id) REFERENCES product(id)
);